### hap.py singularity

Creating a [singularity](https://singularity.lbl.gov/index.html) image to use [hap.py](https://github.com/Illumina/hap.py) in TSD, and scripts related to such.
