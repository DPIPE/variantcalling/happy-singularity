#!/bin/bash
# Job name:
#SBATCH --job-name=hap.py

# Use the express queue
#SBATCH --reservation=amg

# Project:
#SBATCH --account=p22

# Wall clock limit:
#SBATCH --time=02:00:30

# Memory
#SBATCH --mem-per-cpu=3140

# Number of CPUs
#SBATCH --cpus-per-task=2
set -e
if [ -f /cluster/bin/jobsetup ]; then
	source /cluster/bin/jobsetup
fi

function error_msg() {
	echo
	echo " *** ERROR ***"
	echo -e "$1"
	echo
	exit 1
}

function usage() {
	if [[ ! -z $1 ]]; then
		echo
		echo -e "$1"
	fi
	echo
	echo " Usage: $0 < SAMPLE_ID > [ DATA_DIR ]"
	echo
	echo "SAMPLE_ID - Full sample ID of target. e.g., Diag-Excap999-NA12878"
	echo "DATA_DIR  - Specify exact location of sample's data. Use if not on /cluster"
	echo
	exit 1
}

SINGULARITY_VER=3.3.0
HAPPY_VERSION=v0.2.0

cd $SCRATCH
if [[ "$(which singularity 2>/dev/null)x" == "x" ]]; then
	module load singularity/$SINGULARITY_VER
fi

if [[ "$(which jq 2>/dev/null)x" == "x" ]]; then
	JQ=/cluster/projects/p22/sw/bin/jq
else
	JQ=jq
fi

# Force USE_CLUSTER until it's possible to use from non-/cluster accessible VMs
USE_CLUSTER=1
if [[ -z $USE_CLUSTER ]]; then
	DUR_BASE=/tsd/p22/data/durable
	DUR_PROD=$DUR_BASE/production
	DUR2_BASE=/tsd/p22/data/durable2
	DUR2_PROD=$DUR2_BASE/production
	ADIR=($DUR_PROD/analyses $DUR2_PROD/analyses)
	SDIR=($DUR_PROD/samples $DUR2_PROD/samples)
	SINGULARITY_BASE=$DUR2_BASE/singularity
else
	CLUSTER=/cluster/projects/p22/production
	SDIR=($CLUSTER/samples)
	ADIR=($CLUSTER/analyses)
	SINGULARITY_BASE=${CLUSTER/production/singularity}
fi
SINGULARITY_DIR=$SINGULARITY_BASE/happy
HAPPY_IMAGE=$SINGULARITY_DIR/happy_${HAPPY_VERSION}.sif

BUNDLE=${BUNDLE:-/cluster/projects/p22/production/sw/vcpipe/vcpipe-bundle}
BUNDLE_JSON=$BUNDLE/bundle.json

if [[ -z $HAPPY_IMAGE ]] || [[ ! -f $HAPPY_IMAGE ]]; then
	error_msg "no happy singularity image found, check $SINGULARITY_DIR"
fi

if [[ -z $1 ]]; then
	usage "You must give a sample_id to run happy on\ne.g., Diag-EKG123456-NA12878 or Diag-EKG123456-NA12878-KREFT-v03"
elif [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]] || [[ "$1" == "help" ]]; then
	usage
fi
SAMPLE_ID_FULL=$1
SAMPLE_ID=$(echo $1 | cut -f 1-3 -d-)
SAMPLE_ID_SHORT=$(echo $1 | cut -f 3 -d-)

if [[ ! -z $2 ]]; then
	DATA_DIR=$2
fi

for SD in ${SDIR[*]}; do
	if [[ -d "$SD/$SAMPLE_ID" ]]; then
		SAMP_DIR="$SD/$SAMPLE_ID"
		break
	elif [[ $(ls -d $SD/$SAMPLE_ID* 2>/dev/null | wc -l) == "1" ]]; then
		SAMP_DIR=$(ls -d $SD/$SAMPLE_ID*)
		break
	fi
done

if [[ -z $SAMP_DIR ]]; then
	error_msg "Can't find sample directory for $SAMPLE_ID in ${SDIR[*]}, bailing"
else
	echo "Using SAMP_DIR=$SAMP_DIR"
fi

if [[ -z $DATA_DIR ]]; then
	for AD in ${ADIR[*]}; do
		if [[ -d "$AD/$SAMPLE_ID_FULL" ]]; then
			BASE_DATA_DIR="$AD/$SAMPLE_ID_FULL"
			break
		elif [[ -d "$AD/$SAMPLE_ID" ]]; then
			BASE_DATA_DIR="$AD/$SAMPLE_ID"
			break
		fi
	done
	if [[ -z $BASE_DATA_DIR ]]; then
		error_msg "Can't find analysis directory for $SAMPLE_ID_FULL or $SAMPLE_ID in ${ADIR[*]}"
	else
		echo "Using BASE_DATA_DIR=$BASE_DATA_DIR"
	fi

	pushd $BASE_DATA_DIR/result
	LATEST_RUN=$(ls -t | head -1)
	if [[ ! -d $LATEST_RUN ]]; then
		error_msg "Found non-directory in $BASE_DATA_DIR/result: $LATEST_RUN"
	fi
	LATEST_NOCOLON=${LATEST_RUN//:/-}
	if [[ -z $LATEST_RUN ]]; then
		error_msg "No analysis directory found in $BASE_DATA_DIR/result"
	elif [[ "$LATEST_RUN" != "$LATEST_NOCOLON" ]]; then
		ln -s $LATEST_RUN $LATEST_NOCOLON
		LATEST_RUN=$LATEST_NOCOLON
	fi
	DATA_DIR=$BASE_DATA_DIR/result/$LATEST_RUN
	popd
elif [[ ! -d $DATA_DIR ]]; then
	error_msg "User supplied DATA_DIR ($DATA_DIR) could not be found\nCorrect the value, or unset it to find it automatically"
else
	echo "Using user supplied DATA_DIR"
fi
echo "Using DATA_DIR=$DATA_DIR"

SAMPLE_JSON=$(ls $SAMP_DIR/*.sample | head -1)
if [[ ! -f $SAMPLE_JSON ]]; then
	error_msg "Unable to find .sample file in $SAMP_DIR"
fi
CAPTURE_KIT=$(cat $SAMPLE_JSON | $JQ -r .capturekit)

# Check if NA12878 or HG00{2,3,4} and use the appropriate files
# There are sometimes suffixes (e.g., NA12878N7), so substr to get the base ID
if [[ ${SAMPLE_ID_SHORT:0:7} == "NA12878" ]]; then
	HC_REGION=$BUNDLE/$(cat $BUNDLE_JSON | $JQ -r .regression.HIGH_CONF_CALL_ORI)
	TRUTH_VCF=$BUNDLE/$(cat $BUNDLE_JSON | $JQ -r .regression.HIGH_CONF_CALL_ORI_HCREGION)
elif [[ ${SAMPLE_ID_SHORT:0:4} == "HG00" ]]; then
	HG_BASEDIR=$BUNDLE/regression/AshkenazimTrio
	HG_SID=${SAMPLE_ID_SHORT:0:5}
	case $HG_SID in
		HG002)
			HG_BASEDIR=$HG_BASEDIR/HG002_NA24835_son/v3.3
			HC_REGION=$HG_BASEDIR/${HG_SID}_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-Solid-10X_CHROM1-22_v3.3_highconf.bed
			TRUTH_VCF=$HG_BASEDIR/${HG_SID}_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-Solid-10X_CHROM1-22_v3.3_highconf.vcf.gz
			;;
		HG003)
			HG_BASEDIR=$HG_BASEDIR/HG003_NA24149_father/v3.3
			HC_REGION=$HG_BASEDIR/${HG_SID}_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X_CHROM1-22_v3.3_highconf.bed
			TRUTH_VCF=$HG_BASEDIR/${HG_SID}_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X_CHROM1-22_v3.3_highconf.vcf.gz
			;;
		HG004)
			HG_BASEDIR=$HG_BASEDIR/HG004_NA24143_mother/v3.3
			HC_REGION=$HG_BASEDIR/${HG_SID}_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X_CHROM1-22_v3.3_highconf.bed
			TRUTH_VCF=$HG_BASEDIR/${HG_SID}_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X_CHROM1-22_v3.3_highconf.vcf.gz
			;;
		*)
			error_msg "Received HG ID without a truthset: $HG_SID"
			;;
	esac
else
	error_msg "Unable to parse sample ID ($SAMPLE_ID_SHORT) to find truthset"
fi


if [[ "$CAPTURE_KIT" == "wgs" ]]; then
	TARGET_REGION=$BUNDLE/$(cat $BUNDLE_JSON | $JQ -r .regression.HIGH_CONF_CALL_ORI)
else
	echo "Generating target region file for capture kit $CAPTURE_KIT"
	echo "HC_REGION=$HC_REGION ($(wc -l $HC_REGION|cut -f1 -d' '))"

	TARGET_REGION=high_conf_intersect.bed

	KIT_REGIONS=$BUNDLE/$(cat $BUNDLE_JSON | $JQ -r .captureKit.${CAPTURE_KIT}.noslop_bed)
	echo "KIT_REGIONS=$KIT_REGIONS ($(wc -l $KIT_REGIONS | cut -f1 -d' '))"

	if [[ $(echo $CAPTURE_KIT | cut -c1-5) == "CuCaV" ]]; then
		PANEL_REGIONS=$BUNDLE/clinicalGenePanels/KREFT_v04/KREFT_v04.codingExons.slop20.bed
	else
		PANEL_REGIONS=$BUNDLE/$(cat $BUNDLE_JSON | $JQ -r .captureKit.${CAPTURE_KIT}.calling_region)
	fi
	echo "PANEL_REGIONS=$PANEL_REGIONS ($(grep -vc ^@ $PANEL_REGIONS))"
	echo

	if [[ "$(which bedtools 2>/dev/null)x" == "x" ]]; then
		module load bedtools || error_msg "Couldn't load bedtools"
	fi
	bedtools multiinter -i $HC_REGION \
		$KIT_REGIONS \
		<(grep -v ^@ $PANEL_REGIONS) \
	    | perl -wlane 'print join("\t",@F[0..2]) if ($F[3] > 2)' \
		> $TARGET_REGION
fi
echo "Using TARGET_REGION=$TARGET_REGION ($(wc -l $TARGET_REGION | cut -f1 -d' ') lines)"

# use data stored in p22-torps until bundle update with happy ref data is in master
# HAPPY_REF=$BUNDLE/$(cat $BUNDLE_JSON | $JQ -r .reference.happy)
HAPPY_REF=/cluster/projects/p22/dev/p22-torps/hap.py-data
VCF_DIR=$DATA_DIR/data/variantcalling
VCF_FILE=$VCF_DIR/${SAMPLE_ID_FULL}.final.vcf
if [[ ! -f $VCF_FILE ]]; then
	error_msg "Unable to find final VCF in $VCF_DIR\nTried: $VCF_FILE"
fi
OUTPUT_HOST=$DATA_DIR/data/qc
OUTPUT_IMG=/results/$SAMPLE_ID_FULL

SINGULARITY_BINDPATH="$TARGET_REGION:/data/target_region.bed"
SINGULARITY_BINDPATH="$SINGULARITY_BINDPATH,${VCF_FILE}:/data/sample.vcf"
SINGULARITY_BINDPATH="$SINGULARITY_BINDPATH,${TRUTH_VCF}:/data/truth.vcf.gz"
SINGULARITY_BINDPATH="$SINGULARITY_BINDPATH,${HAPPY_REF}:/opt/hap.py-data"
SINGULARITY_BINDPATH="$SINGULARITY_BINDPATH,$OUTPUT_HOST:/results"
export SINGULARITY_BINDPATH
echo "Using SINGULARITY_BINDPATH:"
echo $SINGULARITY_BINDPATH | tr , "\n"
echo

PROC_HOME=$(mktemp -dp $PWD)
singularity exec --cleanenv -H $PROC_HOME:/home ${HAPPY_IMAGE} hap.py \
            /data/truth.vcf.gz \
            /data/sample.vcf \
            -T /data/target_region.bed \
            -o $OUTPUT_IMG
