#!/usr/bin/env python3

import argparse
import gzip
import io
import os
from pyensembl import EnsemblRelease
import re
import sys
import vcf


GRCH37 = EnsemblRelease(75)
TRUTH = 0
QUERY = 1


def main():
    parser = argparse.ArgumentParser(description="Merge hap.py output VCFs into a single csv for review")
    parser.add_argument("-f", "--files", nargs="+", required=True, help="List of hap.py VCFs to merge")
    parser.add_argument(
        "-c", "--coverage", action="store_true", help="Get coverage info from vcpipe outputted VCF files"
    )
    parser.add_argument("--verbose", action="store_true", help="be extra chatty")
    parser.add_argument("--debug", action="store_true", help="run in debug mode")
    args = parser.parse_args()

    if args.debug:
        setattr(args, "verbose", True)

    sample_names = list(map(lambda x: re.sub("\..+$", "", x), args.files))
    tbl = dict()
    for fname in args.files:
        reader = vcf.Reader(filename=fname, compressed=True)
        for var in reader:
            gene_resp = GRCH37.genes_at_locus(contig=var.CHROM, position=var.POS)
            var_genes = [g.gene_name for g in gene_resp]
            var_type = var.var_type.upper()
            slug = "{}\t{}\t{}\t{}/{}\t{}".format(
                var.CHROM, ", ".join(var_genes), var.POS, var.REF, ",".join(str(x) for x in var.ALT), var_type
            )
            # slug = (var.CHROM, var_genes, var.POS, var.REF, var.ALT, var_type)

            if var.samples[TRUTH].data.BD == "TP":
                status = "TP"
            elif var.samples[QUERY].data.BD == "FP":
                status = "FP"
            elif var.samples[TRUTH].data.BD == "FN":
                status = "FN"
            else:
                status = "??? {}".format(var.samples[QUERY].data)

            if slug not in tbl:
                tbl[slug] = {}

            tbl[slug][fname] = status

    if args.coverage:
        print("coverage per sample not implemented")
        # for sammp in sample_names:
        #     anno_fname = f"{samp}.final.vcf.gz"
        #     if not os.path.exists(anno_fname):
        #         anno_fname = f"{samp}.g.vcf.gz"
        #         if not os.path.exists(anno_fname):
        #             print(f"Unable to find annotation vcf for sample {samp}, skipping", file=sys.stderr)
        #             continue
        #             reader = vcf.Reader(filename=anno_fname, compressed=True)
        #             for var in tbl.keys():
        #             calls = reader.fetch(var.CHROM, var.POS-1, var.POS)
        #             for call in calls:
        #                 if call.CHROM == var.CHROM and call.POS == var.POS and call.var_type == var.var_type:
        #                     var['coverage'] = call.INFO.get("DP", -1)

    outfile = "happy_summary.csv"
    with io.open(outfile, "w") as output:
        output.write("\t".join(["chr", "gene(s)", "pos", "ref/alt", "type"] + sample_names) + "\n")
        for var in sorted(tbl.keys()):
            output_line = "\t".join([var] + list(map(lambda x: tbl[var].get(x, "not called"), args.files)))
            output.write("{}\n".format(output_line))
    print("Output written to {}".format(outfile))


###


if __name__ == "__main__":
    main()
