#!/usr/bin/env python3

import argparse
import csv
import datetime
import glob
import json
import os
import re
import sys


### (mostly) static globals
CONTROL_SAMPLES = ["NA12878", "HG002", "HG003", "HG004"]
ADIRS = [
    "/cluster/projects/p22/production/analyses",
    "/tsd/p22/data/durable2/production/analyses",
    "/tsd/p22/data/durable/production/analyses",
    "/tsd/p22/data/durable2/production/preprocessed/singles",
]
OUTPUT_COLS = [
    "Sample",
    "SNP Sensitivity * 100",
    "SNP Sensitivity",
    "SNP True Positives",
    "SNP False Negatives",
    "SNP False Positives",
    "INDEL Sensitivity * 100",
    "INDEL Sensitivity",
    "INDEL True Positives",
    "INDEL False Negatives",
    "INDEL False Positives",
    "PCT_TARGET_BASES_10X",
    "PCT_TARGET_BASES_20X",
    "MEAN_TARGET_COVERAGE",
    "skewedRatio",
    "tiTvRatio",
    "number of variants",
    "Q30% of reads",
]
COL_MAP = {
    "happy": {
        "Sensitivity * 100": "METRIC.Recall",
        "Sensitivity": "METRIC.Recall",
        "True Positives": "TRUTH.TP",
        "False Negatives": "TRUTH.FN",
        "False Positives": "QUERY.FP",
    },
    "qc_result": {
        "PCT_TARGET_BASES_10X": ("CalculateHsMetrics", "PCT_TARGET_BASES_10X"),
        "PCT_TARGET_BASES_20X": ("CalculateHsMetrics", "PCT_TARGET_BASES_20X"),
        "MEAN_TARGET_COVERAGE": ("CalculateHsMetrics", "MEAN_TARGET_COVERAGE"),
        "skewedRatio": ("VcfQuality", "skewedRatio"),
        "tiTvRatio": ("VcfQuality", "tiTvRatio"),
        "number of variants": ("VcfQuality", "variants"),
        "Q30% of reads": ("BaseQuality", "q30_bases_pct"),
    },
}


### kgo


def main():
    global ADIRS
    parser = argparse.ArgumentParser()
    sample_group = parser.add_mutually_exclusive_group(required=True)
    sample_group.add_argument("-p", "--projects", metavar="PROJECT_ID", nargs="+", help="project(s) to validate")
    sample_group.add_argument("-i", "--individuals", metavar="IND_ID", nargs="+", help="individual(s) to validate")
    parser.add_argument("-o", "--output", required=True, help="output file name")
    parser.add_argument("-s", "--include-samples", action="store_true", help="include non-control samples in output")
    parser.add_argument(
        "-a",
        "--analysis-dir",
        metavar="DIR_PATH",
        nargs="+",
        help="add another location to look for analyses, default: {}".format(", ".join(ADIRS)),
    )
    parser.add_argument("--verbose", action="store_true", help="be extra chatty")
    parser.add_argument("--debug", action="store_true", help="run in debug mode")
    args = parser.parse_args()

    if args.debug:
        setattr(args, "verbose", True)

    if args.analysis_dir:
        ADIRS = args.analysis_dir + ADIRS
        if args.verbose:
            print("Prepended {} to analysis dir list".format(args.analysis_dir))

    if re.search(string=args.output, pattern="\.(?:csv|txt|tab|xlsx?)$", flags=re.I):
        output_filename = args.output
    else:
        output_filename = args.output + ".xls"

    if args.debug:
        print("args: {}".format(args))

    if args.verbose:
        if args.projects:
            log("{} Checking projects ({}) for control samples to validate".format(now(), ", ".join(args.projects)))
        else:
            log("{} Looking for individuals ({}) to validate".format(now(), ", ".join(args.individuals)))

    samps = {}
    if args.projects:
        if args.include_samples:
            samp_re = re.compile("Diag-(?:{})(?:-[^/-]+)+(?:-[PFM][MK])?$".format("|".join(args.projects)), re.I)
        else:
            samp_re = re.compile(
                "Diag-(?:{})-(?:{})[^\-]*?(?:-[PFM][MK])?$".format("|".join(args.projects), "|".join(CONTROL_SAMPLES)),
                re.I,
            )
    else:
        # Only look for specified individuals
        samp_re = re.compile(
            "Diag-(?:{})(?:-[PFM][MK])?$".format(
                "|".join(list(set([x.replace("Diag-", "") if x.startswith("Diag-") else x for x in args.individuals])))
            )
        )
    if args.debug:
        print("Using regex string: {}".format(samp_re))

    for adir in ADIRS:
        if args.verbose:
            log("Now scanning {}".format(adir))
        for samp_dir in os.scandir(adir):
            if args.debug:
                print("Checking sample dir {}".format(samp_dir.name))
            if samp_dir.is_file():
                continue
            if re.search(pattern=samp_re, string=samp_dir.name):
                if not os.path.isdir(os.path.join(samp_dir.path, "result")) and not os.path.isdir(
                    os.path.join(samp_dir.path, "data")
                ):
                    continue
                if samp_dir.name in samps:
                    if args.verbose:
                        log("found duplicate match for {}, skipping".format(samp_dir.name))
                    continue
                if args.debug:
                    log("matched {} with {}".format(samp_dir.name, samp_re))
                samps[samp_dir.name] = samp_dir

    if args.verbose:
        print("Found {} matching samples".format(len(samps)))

    output_rows = []
    for basedir in sorted(samps.keys()):
        samp_name = basedir.replace("Diag-", "")
        if os.path.isdir(os.path.join(samps[basedir].path, "data")):
            qc_path = os.path.join(samps[basedir].path, "data", "qc")
        else:
            res_path = os.path.join(samps[basedir].path, "result")
            latest_result = sorted(os.scandir(res_path), key=lambda x: x.stat().st_ctime, reverse=True)[0].path
            qc_path = os.path.join(latest_result, "data", "qc")

        # make sure happy file exists
        happy_summary = glob.glob("{}/*.summary.csv".format(qc_path))
        if args.debug:
            log("Checking for hap.py files for {} in {}".format(samp_name, qc_path))

        # non-control samples won't have hap.py data, but should be export when using --include-samples
        if args.include_samples and "NA12878" not in samp_name and "HG00" not in samp_name:
            happy_summary = None
        elif happy_summary is None or len(happy_summary) == 0:
            print("Could not find hap.py results for {}: has it been run?".format(samp_name), file=sys.stderr)
            continue
        elif len(happy_summary) > 1:
            print(
                "Found multiple possible hap.py summary.csv files for {}: remove any duplicates and try again".format(
                    samp_name
                ),
                file=sys.stderr,
            )
            continue
        else:
            happy_summary = happy_summary[0]

        # make sure qc_result.json exists
        if args.debug:
            log("Checking for qc_result.json for {} in {}".format(samp_name, qc_path))
        qc_result = glob.glob("{}/*qc_result.json".format(qc_path))
        if len(qc_result) == 0:
            print("Could not find qc_result.json file for {}, skipping".format(samp_name))
            continue
        elif len(qc_result) > 1:
            print("Found multiple qc_result files for {} in {}, skipping".format(samp_name, qc_path))
            continue
        else:
            qc_result = qc_result[0]

        # setup the row and start processing the files
        row = {x: None for x in OUTPUT_COLS}
        row[OUTPUT_COLS[0]] = samp_name

        # happy, go!
        if happy_summary:
            with open(happy_summary) as infile:
                rdr = csv.DictReader(infile)
                for happy_row in rdr:
                    if happy_row["Filter"] == "ALL":
                        for field_name in COL_MAP["happy"].keys():
                            colname = "{} {}".format(happy_row["Type"], field_name)
                            field_value = happy_row.get(COL_MAP["happy"][field_name], 0)
                            if field_name == "Sensitivity":
                                field_value = float(field_value)
                            elif field_name == "Sensitivity * 100":
                                field_value = float(field_value) * 100
                            else:
                                field_value = int(field_value)
                            row[colname] = field_value
        else:
            # non-control samples don't have hap.py data, but still need keys for csv.DictWriter
            for coltype in ("SNP", "INDEL"):
                for field_name in COL_MAP["happy"].keys():
                    colname = "{} {}".format(coltype, field_name)
                    row[colname] = None

        # qc_result, go!
        with open(qc_result) as infile:
            qc_obj = json.load(infile)
            for field_name in COL_MAP["qc_result"].keys():
                key_name = COL_MAP["qc_result"][field_name][0]
                qc_field = COL_MAP["qc_result"][field_name][1]
                try:
                    field_value = qc_obj[key_name][1][qc_field]
                except KeyError as e:
                    if key_name == "VcfQuality":
                        field_value = qc_obj["SmallVcfQuality"][1][qc_field]
                    else:
                        raise e
                row[field_name] = field_value

        output_rows.append(row)

    # make sure we have stuff to write
    if len(output_rows) > 0:
        with open(output_filename, "w") as outfile:
            writer = csv.DictWriter(outfile, OUTPUT_COLS)
            writer.writeheader()
            for r in output_rows:
                writer.writerow(r)

        log("Successfully wrote {} samples to {}".format(len(output_rows), output_filename))
    else:
        print(
            "Could not find any control samples with hap.py and qc_result.json files for these projects: {}".format(
                ", ".join(args.projects)
            )
        )


def log(msg):
    print("{} {}".format(now(), msg))


def now():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


###


if __name__ == "__main__":
    main()
