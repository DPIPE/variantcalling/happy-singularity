## Description

Fixes issue: 

## Checklist

- [ ] Refers to a Jira issue

- [ ] This MR has a description (including any risks) giving the reviewer context

- [ ] Tests have been done (please state how changes has been tested)

- [ ] The vcpipe/config/nextflow.config has been updated

- [ ] The vcpipe release version together with this change:

